package activity.admobs.imdroid.animvidtest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;

public class Sample extends AppCompatActivity {
RelativeLayout relativeLayout;
    Animation fromleft;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample);
        relativeLayout = (RelativeLayout)findViewById(R.id.samples);
        fromleft = AnimationUtils.loadAnimation(this, R.anim.fromleft);
       // relativeLayout.startAnimation(fromleft);

    }
}
