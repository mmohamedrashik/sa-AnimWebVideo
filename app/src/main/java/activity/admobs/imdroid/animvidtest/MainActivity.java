package activity.admobs.imdroid.animvidtest;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import it.sephiroth.android.library.picasso.Picasso;

public class MainActivity extends AppCompatActivity {
ImageView imageView;
Button button;
    Animation fromleft;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fromleft = AnimationUtils.loadAnimation(this,R.anim.fromleft);
        button = (Button)findViewById(R.id.next);
        button.startAnimation(fromleft);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), Sample.class);
                startActivity(intent);
                overridePendingTransition(R.anim.fromleft,R.anim.fadeout);

            }
        });

      /*  Intent intent = new Intent(getApplicationContext(), Webss.class);
        startActivity(intent); */
        imageView = (ImageView)findViewById(R.id.previewer);
        try
        {
            // videoId=extractYoutubeId("http://www.youtube.com/watch?v=t7UxjpUaL3Y");


            String videoId = extractYTId("https://www.youtube.com/embed/OvH46eOn_kM");

            Log.e("VideoId is->","" + videoId);

            String img_url="http://img.youtube.com/vi/"+videoId+"/mqdefault.jpg"; // this is link which will give u thumnail image of that video

            // picasso jar file download image for u and set image in imagview

            Picasso.with(getApplicationContext())
                    .load(img_url)
                    .into(imageView);

        }
        catch (Exception e)
        {
            e.printStackTrace();

            Log.e("error",e.toString());
        }

    }
    public static String extractYTId(String ytUrl) {
        String vId = null;
        Pattern pattern = Pattern.compile(
                "^https?://.*(?:youtu.be/|v/|u/\\w/|embed/|watch?v=)([^#&?]*).*$",
                Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(ytUrl);
        if (matcher.matches()){
            vId = matcher.group(1);
        }

        else{

            Log.d("error", "error");
        }
        return vId;
    }
}
